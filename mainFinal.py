from typing import List, Optional
from fastapi import FastAPI
from pydantic import BaseModel
#importing Models as definitions from the files
from FuzzyMatcherFinal import fuzzyMatch_Def
from fuzzytest import fuzzyMatch_Def_test
from RecordLinkageFinal import recordLinkage_Def
from fuzzymatchchunks import fuzzyMatch_Def4
from Fuzzymatch_singlecolumn import fuzzyMatch_Def_single
#from RecordLinkage_Spark import recordLinkage_Def_Spark

#assigning FastAPI as app
app = FastAPI()

### Fuzzy Matcher algorithm to RestAPI
#Defining Base Model
#defining the inputs
class FuzzyMatcherRequest(BaseModel):
    #driver dataset location
    driverDatasetLocation: str
    #Reference dataset location
    referenceDatasetLocation: str
    #ouput file storage location
    fuzzyMatchedLocation: str
    #accepting delimiters as inputs
    driverDelimiter : str
    referenceDelimiter : str
    resultDelimiter : str
    #cutoff to retrive only data above the given threshold
    threshold: Optional[str]
    #input column form driver dataset and reference dataset
    columnNames: List[dict]
    #driverColumnNames: Optional[List[str]] = Query(None)
    #referenceColumnNames: Optional[List[str]] = Query(None)


#defining the endpoint for fuzzy matcher Model
@app.post("/FuzzyMatchRestAPI2")
#calling the functions with given inputs
async def FuzzyMatchRestAPI2(fm:FuzzyMatcherRequest):
    #Result = fuzzyMatchDef(fm.driverDatasetLocation,fm.referenceDatasetLocation,fm.fuzzyMatchedLocation,fm.threshold,fm.driverColumnNames,fm.referenceColumnNames)
    Result = fuzzyMatch_Def_single(fm.driverDatasetLocation,fm.referenceDatasetLocation,fm.fuzzyMatchedLocation,fm.threshold,fm.columnNames, fm.driverDelimiter, fm.referenceDelimiter, fm.resultDelimiter)
    # ...
    # Runs the fuzzy matcher algorithm
    # ...
    return {f"{Result}"}
#
# @app.post("/FuzzyMatchRestAPI3")
# #calling the functions with given inputs
# async def FuzzyMatchRestAPI2(fm:FuzzyMatcherRequest):
#     #Result = fuzzyMatchDef(fm.driverDatasetLocation,fm.referenceDatasetLocation,fm.fuzzyMatchedLocation,fm.threshold,fm.driverColumnNames,fm.referenceColumnNames)
#     Result = fuzzyMatch_Def4(fm.driverDatasetLocation,fm.referenceDatasetLocation,fm.fuzzyMatchedLocation,fm.threshold,fm.columnNames, fm.driverDelimiter, fm.referenceDelimiter, fm.resultDelimiter)
#     # ...
#     # Runs the fuzzy matcher algorithm
#     # ...
#     return {f"{Result}"}

### Record Linkage algorithm to RestAPI
#defining the inputs
class RecordLinkageRequest(BaseModel):
    #driver dataset location
    driverDatasetLocation: str
    #Reference dataset location
    referenceDatasetLocation: str
    #ouput file storage location
    fuzzyMatchedLocation: str
    #consistent column from driver dataset and reference dataset
    consistentColumn: dict
    #input columns from driver dataset and reference dataset
    columnNames: List[dict]
    # accepting delimiters as inputs
    driverDelimiter: str
    referenceDelimiter: str
    resultDelimiter: str

#defining the endpoint for Record linkage Model
@app.post("/RecordLinkageRestAPI2")
#calling the functions with given inputs
async def RecordLinkageRestAPI2(rl:RecordLinkageRequest):
    Result = recordLinkage_Def(rl.driverDatasetLocation,rl.referenceDatasetLocation,rl.fuzzyMatchedLocation,rl.consistentColumn,rl.columnNames, rl.driverDelimiter, rl.referenceDelimiter, rl.resultDelimiter)
    # ...
    # Runs the Record Linkage algorithm
    # ...
    return {f"{Result}"}

###Spark
# class RecordLinkage_Spark_Request(BaseModel):
#     #driver dataset location
#     driverDatasetLocation: str
#     #Reference dataset location
#     referenceDatasetLocation: str
#     #ouput file storage location
#     fuzzyMatchedLocation: str
#     #consistent column from driver dataset and reference dataset
#     consistentColumn: dict
#     #input columns from driver dataset and reference dataset
#     columnNames: List[dict]
#
# #defining the endpoint for Record linkage Model
# @app.post("/RecordLinkageRestAPI_Spark")
# #calling the functions with given inputs
# async def RecordLinkageRestAPI_Spark(rl:RecordLinkage_Spark_Request):
#     Result = recordLinkage_Def_Spark(rl.driverDatasetLocation,rl.referenceDatasetLocation,rl.fuzzyMatchedLocation,rl.consistentColumn,rl.columnNames)
#     # ...
#     # Runs the Record Linkage algorithm
#     # ...
#     return {f"{Result}"}