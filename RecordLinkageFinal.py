#importing the required libraries and packages
import pandas as pd
import recordlinkage as rl
import re as re

#defining coltomove function to show the input columns as first columns in output
def coltomove(df, cols_move_list=[], ref_col='', place='After'):
    cols = df.columns.tolist()
    if place == 'After':
        a1 = cols[:list(cols).index(ref_col) + 1]
        a2 = cols_move_list
    if place == 'Before':
        a1 = cols[:list(cols).index(ref_col)]
        a2 = cols_move_list + [ref_col]

    a1 = [i for i in a1 if i not in a2]
    a3 = [i for i in cols if i not in a1 + a2]

    return (df[a1 + a2 + a3])

#defining RecordLinkage fuction to take inputs, perform the actions and gives the output
def recordLinkage_Def( driverDatasetLocation, referenceDatasetLocation, fuzzyMatchedLocation, consistentColumn, columnNames, driverDelimiter, referenceDelimiter, resultDelimiter):
    ##Read Driver dataset or left dataset as dataframe
    df_left = pd.read_csv(driverDatasetLocation, delimiter=f"{driverDelimiter}", header='infer')
    #set the index name
    df_left.index.name = 'Index_df_left'
    #convert the datatypes to string
    df_left = df_left.astype(str)
    # Read Reference dataset or right dataset as dataframe
    df_right = pd.read_csv(referenceDatasetLocation, delimiter=f"{referenceDelimiter}", header='infer')
    # set the index name
    df_right.index.name = 'Index_df_right'
    # convert the datatypes to string
    df_right = df_right.astype(str)

    #consistent column names to df_left_column1 and df_right_column1 from driver and reference datasets respectively
    df_left_column1 = consistentColumn["driverColumn"]
    df_right_column1 = consistentColumn["referenceColumn"]

    if df_left_column1 == Null:


    #assigning record linkage index to indexer
    indexer = rl.Index()
    #generating all the possible pairs with full()
    # indexer.full()
    # Block on State type because it has consistent values across datasets
    #if our datasets are large, generating all the possible pairs will be very computationally expensive. To avoid generating all the possible pairs, we should choose one column which has consistent values from both datasets
    indexer.block(left_on=df_left_column1, right_on=df_right_column1)
    # Create candidate pairs
    pairs = indexer.index(df_left, df_right)
    # Create a comparing object
    compare = rl.Compare()

    drop_list = []
    for i,x in enumerate(columnNames):
        #print(str(x['driverColumn']), str(x['referenceColumn']))
        df_left[x['driverColumn']] = df_left[x['driverColumn']].astype(str)
        df_right[x['referenceColumn']] = df_right[x['referenceColumn']].astype(str)
        thres = float(x['threshold'])
        compare.string(x['driverColumn'], x['referenceColumn'], threshold=thres, method='levenshtein', label=f'columnNames{i}')
        drop_list.append(f'columnNames{i}')
        value1 = i
    #Extracting the columNnames of driver dataset which came as inputs to drivercolumnnames list
    drivercolumnnames = []
    for x in columnNames:
        drivercolumnnames.append(x['driverColumn'])
    # Extracting the columnNames of refernce dataset which came as inputs to drivercolumnnames list
    referencecolumnnames = []
    for y in columnNames:
        referencecolumnnames.append(y['referenceColumn'])

    #generating pairs by comparing the columns
    features = compare.compute(pairs, df_left, df_right)
    #creating score column to show the count of matches for given columns by row
    features['Score'] = features.loc[:,].sum(axis=1)
    #sorting the column sort by value and selecting only the columns which has all the columns matches(value1)
    df_matches = features[features.Score >= value1+1].reset_index()
    #inital merge with potential matches
    initial_merge = df_matches.merge(df_left, left_on='Index_df_left', right_on='Index_df_left', how='left')
    #Final merge of driver and reference dataset
    final_merge = initial_merge.merge(df_right, left_on='Index_df_right', right_on='Index_df_right', how='left')
    # droping the columns which were created in the flow
    final_merge = final_merge.drop(columns = drop_list)
    #droping the columns which were created in the flow
    final_merge = final_merge.drop(columns = ['Index_df_left','Index_df_right'])
    # combining all the input column names into Mathcedcolumns list to move these columns to front of the dataset. For visual appeal
    MatchedColumns = []
    if list(drivercolumnnames) == list(referencecolumnnames):
        for j in columnNames:
            append_dri_columnnames = '_x'
            append_ref_columnnames = '_y'
            MatchedColumns.extend([j['driverColumn'] + append_dri_columnnames, j['referenceColumn'] + append_ref_columnnames])
    else:
        for k in columnNames:
            MatchedColumns.extend([k['driverColumn'], k['referenceColumn']])
    #move columns which came as input to first of the dataset
    final_merge = coltomove(final_merge, cols_move_list= MatchedColumns, ref_col='Score', place='After')
    #sorting dataset by consistent column values of driver dataset
    final_merge = final_merge.sort_values(by=df_left_column1, ascending=False)
    #sorting the dataset by score
    final_merge = final_merge.sort_values(by=['Score'], ascending=False)
    final_merge = final_merge.set_index('Score')
    #final dataset to csv and giving the location to save
    final_merge.to_csv(fuzzyMatchedLocation, sep=f'{resultDelimiter}')
    return "Successfully ran the Record Linkage"
