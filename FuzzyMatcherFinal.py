#importing the required libraries and packages
import fuzzymatcher
import pandas

#defining coltomove function to show the input columns as first columns in output
def coltomove(df, cols_move_list=[], ref_col='', place='After'):
    cols = df.columns.tolist()
    if place == 'After':
        a1 = cols[:list(cols).index(ref_col) + 1]
        a2 = cols_move_list
    if place == 'Before':
        a1 = cols[:list(cols).index(ref_col)]
        a2 = cols_move_list + [ref_col]

    a1 = [i for i in a1 if i not in a2]
    a3 = [i for i in cols if i not in a1 + a2]

    return (df[a1 + a2 + a3])


# Python program to convert a list to string

# Function to convert
def listToString(s):
    # initialize an empty string
    str1 = ""

    # traverse in the string
    for ele in s:
        str1 += ele

        # return string
    return str1

#defining fuzzymatcher fuction to take inputs, perform the actions and gives the output
def fuzzyMatch_Def(driverDatasetLocation, referenceDatasetLocation, fuzzyMatchedLocation, threshold, columnNames, driverDelimiter, referenceDelimiter, resultDelimiter, splitcondition):
    print("Running Fuzzy Matcher")
    #Read Driver dataset or left dataset as dataframe, as the file is in csv take delimiter as input and index_col 0 so that no index is generated when csv is read as dataframe
    DriverDataset = pandas.read_csv(driverDatasetLocation, delimiter=f"{driverDelimiter}", header='infer', index_col=[0])
    #DriverDataset = DriverDataset.iloc[:20000, :]
    #Read Reference dataset or right dataset as dataframe
    ReferenceData = pandas.read_csv(referenceDatasetLocation, delimiter=f"{referenceDelimiter}", header='infer', index_col=[0])
    #Extracting the columnnames of driver dataset which came as inputs to drivercolumnnames list

    drivercolumnnames = []
    for i,x in enumerate(columnNames):
        #DriverDataset[x['driverColumn']] = DriverDataset[x['driverColumn']].astype(str)
        drivercolumnnames.append(x['driverColumn'])
        numofinputs = i+1
    #Extracting the columnnames of reference dataset which came as inputs to referencecolumnnames list
    referencecolumnnames = []
    for y in columnNames:
        #ReferenceData[x['referenceColumn']] = ReferenceData[x['referenceColumn']].astype(str)
        referencecolumnnames.append(y['referenceColumn'])
    #appending _x for input columns of reference dataset in the orginal dataset
    #i is the number of input column came from each dataset.
    for i in range(numofinputs):
        ReferenceData = ReferenceData.rename(columns=lambda z: z + '_x' if z in referencecolumnnames else z)
    # appending _x for input columns of reference dataset in the input columnnames list
    append_str = '_x'
    # Append suffix to strings in list
    referencecolumnnames = [sub + append_str for sub in referencecolumnnames]
    print("Received columns")

    # if len(drivercolumnnames) == 1:
    #     drivercolumnnames = listToString(drivercolumnnames)
    #     print

    #run the fuzzy matcher algorithm
    Join_FuzzyMatched = fuzzymatcher.fuzzy_left_join(DriverDataset, ReferenceData, drivercolumnnames, referencecolumnnames)
    print("ranfuzzymatcher")

    # copy the data and drop __id_left and __id_right which are the columns generated when fuzzy matcher algorithm is run
    Join_FuzzyMatched_copy = Join_FuzzyMatched.drop('__id_left', 1)
    Join_FuzzyMatched_copy = Join_FuzzyMatched_copy.drop('__id_right', 1)
    #combining all the input column names into Mathcedcolumns list to move these columns to front of the dataset. For visual appeal
    MatchedColumns = []
    for j in columnNames:
        append_ref_columnnames = '_x'
        MatchedColumns.extend([j['driverColumn'], j['referenceColumn'] + append_ref_columnnames])
    Join_FuzzyMatched_copy = coltomove(Join_FuzzyMatched_copy, cols_move_list=MatchedColumns, ref_col='best_match_score', place='After')

    # apply normalization techniques on Column best_match_score so that the values are in range 1 to -1
    Join_FuzzyMatched_copy['best_match_score'] = Join_FuzzyMatched_copy['best_match_score'] / Join_FuzzyMatched_copy['best_match_score'].abs().max()
    #sorting the dataset by best_match_score scores
    Join_FuzzyMatched_copy = Join_FuzzyMatched_copy.sort_values(by=['best_match_score'], ascending=False)
    #setting the best_match_score as index column for the fuzzy dataset
    Join_FuzzyMatched_copy = Join_FuzzyMatched_copy.set_index('best_match_score')
    print("outputisready")
    #Threshold from input if none all the dataset is saved in the output location else only the data above the threshold of best_match_score is saved in the output location.
    if threshold is None:
        Join_FuzzyMatched_copy.to_csv(fuzzyMatchedLocation, sep=f'{resultDelimiter}')
    else:
        bestmatch = Join_FuzzyMatched_copy.query(f'best_match_score >= {threshold}')
        bestmatch.to_csv(fuzzyMatchedLocation, sep=f'{resultDelimiter}')
    # after successfully executing the algorithm print success statement
    return "Successfully ran the Fuzzy Match"